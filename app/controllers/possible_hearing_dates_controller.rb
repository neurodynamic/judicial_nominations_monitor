class PossibleHearingDatesController < ApplicationController
  # http_basic_authenticate_with name: ENV['APPLICATION_USERNAME'], password: ENV['APPLICATION_PASSWORD']

  def index
    @hearings = PossibleHearingDate.all
  end

  def show
    @possible_hearing_date = PossibleHearingDate.find(params[:id])
  end
end
