Rails.application.routes.draw do
  root 'possible_hearing_dates#index'

  resources :possible_hearing_dates, only: [:index, :show]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
