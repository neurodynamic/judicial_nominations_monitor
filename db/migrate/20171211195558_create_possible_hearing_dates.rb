class CreatePossibleHearingDates < ActiveRecord::Migration[5.1]
  def change
    create_table :possible_hearing_dates do |t|
      t.date :date

      t.timestamps
    end
  end
end
