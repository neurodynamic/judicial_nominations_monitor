class CreateSnapshots < ActiveRecord::Migration[5.1]
  def change
    create_table :snapshots do |t|
      t.string :url
      t.references :possible_hearing_date

      t.timestamps
    end
  end
end
