class AddHtmlToSnapshots < ActiveRecord::Migration[5.1]
  def change
    add_column :snapshots, :html, :text
  end
end
