# To be run once per day
task scan_for_nominee_hearing_page: :environment do

  print "Finding or creating hearing date..."
  hearing = PossibleHearingDate.find_or_create_by(date: upcoming_wednesday)
  puts "done."

  print "Getting possible hearing date page's html content..."
  judiciary_response = HTTParty.get(judiciary_url_for(hearing.date))
  page_html = judiciary_response.response.body
  puts "done."

  print "Generating request URL..."
  screenshot_request_url = screenshotlayer_request_url_for(upcoming_wednesday)
  puts "request url complete."
  puts "Screenshot request url: #{(screenshot_request_url).to_s}"
  puts

  print "Sending request to generate screenshot and upload it to imgur..."
  response = HTTParty.post(
    "https://api.imgur.com/3/image",
    query: {
      image: screenshot_request_url,
      type: "URL",
      title: "Committee on the Judiciary Nominee Hearing Page for #{upcoming_wednesday.strftime("%b %-d, %Y")}, screenshot from #{Time.now.strftime('%l:%M%P, %A, %B %-d, (%z %Z)')}",
      description: "Web Address: #{judiciary_url_for(hearing.date)}\n\nScreenshot Taken At: #{Time.now.strftime('%l:%M%P, %A, %B %-d, (%z %Z)')}"
    },
    headers: {
      authorization: "Client-ID #{ENV['IMGUR_CLIENT_ID']}"
    }
  )
  image_url = response.parsed_response["data"]["link"]
  puts "request response received. Uploaded image exists at: #{image_url}"
  puts

  print "Creating snapshot for hearing date on #{hearing.date} with id #{hearing.id}..."
  snapshot = hearing.snapshots.create(url: image_url, html: page_html)
  print "Snapshot with id #{snapshot.id} created. Link: #{snapshot.url}"
end

def upcoming_wednesday
  wednesday = 3

  if Date.today.wday == wednesday
    Date.today
  else
    date_of_next('Wednesday')
  end
end

def screenshotlayer_request_url_for(date)
  judiciary_url = judiciary_url_for(date)

  url = "http://api.screenshotlayer.com/api/capture"
  url += "?access_key=#{ENV['SCREENSHOTLAYER_API_KEY']}"
  url += "&url=#{judiciary_url}"
  url += "&viewport=1440x900"
  url += "&fullpage=1"
  url += "&force=1"
  url += "&width=1440"
  url
end

def judiciary_url_for(date)
  "https://www.judiciary.senate.gov/meetings/#{date.strftime('%m/%d/%Y')}/nominations"
end

def date_of_next(day)
  date  = Date.parse(day)
  delta = date > Date.today ? 0 : 7
  date + delta
end